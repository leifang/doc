# 动态码支付
动态码支付，是渠道商根据用户订单生成对应支付二维码，用户打开支付宝或者微信扫一扫功能，进行扫码支付。
渠道接入商接入流程： 
 
- 明付平台分配给渠道商，对应的渠道号，虚拟门店号，款台号（门店号，款台号根据接入商对账要求和接入商一起定义规则） 
 
- 接入商调用明付平台【下单】接口，取得对应支付二维码链接和对应平台订单号 
 
- 接入商同时发起轮询【查询】接口，轮询间隔3~5,轮询5次，如果仍然没有收到支付确认，请重新【下单】刷新二维码，并提示用户重新扫码支付 
 
- 如果接入商有退款需求，可根据平台订单号进行【退款】操作 

**注意** 暂时不支持支付通知处理 

## 下单生成二维码接口 
 
 - 接口地址： 沙箱：https://realer.cn/api/payService/{version}/unifiedOrder  
    默认版本号 1.0
 
 - 参数列表 

    字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
    --------| ----- | -----|------| ----- | ---
    渠道号  |channelId | 是 |　String | 1000 |  
    门店编号 | storeNo | 是 | String | 100085 |  
    设备编号 | posNo | 是 |　String | 100 | 动态码设备列表区间100~119 
    收银员编号 | cashierNo | 否 | String | 02 | 
    许可列号 | serierNo | 否 | String | fe2323df999 | 动态码请忽略 
    用户ID | openId |　 否　  |  String | oG3werwerwer1221| 动态码请忽略 
    接入商订单号 | orderNo | 否 | String | 1290012 | 接入商不传，默认平台订单号 
    订单金额 | amount | 是 | int | 12 | 单位（分） 
    订单时间 | beginTime | 是 | String  | 20180504123546 | yyyyMMddHHmmss 
    支付类型 |　payType | 是 |  String  | WX_NATIVE | WX_NATIVE(微信）,ALI_NATIVE（支付宝） 
     IP地址 | ip | 是|  String | 127.0.0.1 | 
     币种 | currency | 否 |   String| CNY | 
     商品详情 | goodsDetail | 否  |  String |  |  [{"goodsId":"id0","goodsName":"iname","price":3,"quantity":2},{"goodsId":"id1","goodsName":"iname","price":3,"quantity":2}] 
     备注 | comment |　否 |  String |  |  
     签名 |　sign | 是 |   String | | 非空参数按ASCII 排序，末尾增加明付平台分配的KEY  
       
     
 - 返回结果 
 
     字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
    --------| ----- | -----|------| ----- | ---
    应用ID  | appId | 否 |　String | wxd678efh567hg6787 |  
    时间戳 | timeStamp | 否 | String | 20180504123546 |  
    随机串 | nonceStr | 否 |　String | wxd678efh567hg6787fsdfsd2 | 随机串
    数据包 | package | 否 | String | prepay_id=wx2017033010242291fcfe0db70013231072 | 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=wx2017033010242291fcfe0db70013231072
    签名方式 | signType | 否 | String | MD5 | 签名类型，默认为MD5，支持HMAC-SHA256和MD5。注意此处需与统一下单的签名类型一致 
    签名 | paySign |　 否　  |  String | oG3werwerwer1221|
    明付平台订单号 | tradeNo | 是 | String | P98212781821288121 |  
    第三方平台订单 | outTradeNo | 否 | String | 4001220181230456245 |
    二维码URL | codeUrl | 是 | String  | weixin://123.12121 | **动态码关注字段**  
    
## 订单查询接口 
仅仅支持明付订单号查询 

 - 接口地址： 沙箱： https://realer.cn/api/payService/{version}/tradePayQuery 
    默认版本号 1.0 
    
 - 参数列表 
    
     字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
    --------| ----- | -----|------| ----- | ---
    渠道号  |channelId | 是 |　String | 1000 |  
    门店编号 | storeNo | 是 | String | 100085 |  
    设备编号 | posNo | 是 |　String | 100 | 动态码设备列表区间100~119 
    收银员编号 | cashierNo | 否 | String | 02 | 
    许可列号 | serierNo | 否 | String | fe2323df999 | 动态码请忽略 
    接入商订单号 | orderNo |　 否　  |  String | oG3werwerwer1221| 
    明付商户订单号 | tradeNo | 是 | String | 1290012 | orderNo ,tradeNo 二选一，优先tradeNo
    支付类型 |　payType | 否 |  String  | WX_NATIVE | WX_NATIVE(微信）,ALI_NATIVE（支付宝） 
     IP地址 | ip | 是|  String | 127.0.0.1 | 
     备注 | comment |　否 |  String |  |  
     签名 |　sign | 是 |   String | | 非空参数按ASCII 排序，末尾增加明付平台分配的KEY  
      
 - 返回结果 
 
     字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
    --------| ----- | -----|------| ----- | ---
    原单支付金额  |oriPayMoney | 否 |　int | 1000 |  单位（分），只有查询退款时，该值才有意义
    退款时，原单剩余可退金额 | oriSurplusMoney | 否 | int  | 1000 |  单位（分），只有查询退款时，该值才有意义
    退款时 ，原单接入商订单号 | oriOrderNo | 否 |　String | 201804211320 | 
    退款时，原单明付平台流水号 | oriTradeSerial | 否 | String | P901283232343434 | 
    退款时，原单三方平台流水号 | oriThirdTradeNo | 否 | String | 40092182888812121 |
    总支付金额 | needPayMoney |　 是　  |  int | 1000 | 终端发起的待付金额  needPayMoney=payMoney+thirdDiscountMoney+discountMoney+merchantDiscountMoney
    用户支付金额 | payMoney |　 是　  |  int | 1000 | 用户支付金额 
    第三方优惠金额 | thirdDiscountMoney |　 否　  |  int | 1000 | 第三方优惠金额 
    明付平台优惠金额 | discountMoney |　 否  |  int | 1000 | 明付平台优惠金额  
    接入商优惠金额 | merchantDiscountMoney |　 否　  |  int | 1000 | 暂不支持
    交易时间 | tradeTime |　 是　  |  String | 20180504123546 | yyyyMMddHHmmss   
    接入商订单号 | orderNo | 否 |　String | 201804211320 | 
    明付平台流水号 | tradeSerial | 否 | String | P901283232343434 | 
    三方平台流水号 | thirdTradeNo | 否 | String | 40092182888812121 |  
      
## 订单退款接口 
 
  - 接口地址： 沙箱： https://realer.cn/api/payService/{version}/refundment
     默认版本号 1.0 ，退货要求必须传refundNo,因为可以一票多退，防止重退，该字段接入商户必须保证唯一。参考生成规则orderNo-上笔退款平台订单号
     
  - 参数列表 
  
     字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
    --------| ----- | -----|------| ----- | ---
    渠道号  |channelId | 是 |　String | 1000 |  
    门店编号 | storeNo | 是 | String | 100085 |  
    设备编号 | posNo | 是 |　String | 100 | 动态码设备列表区间100~119 
    收银员编号 | cashierNo | 否 | String | 02 | 
    许可列号 | serierNo | 否 | String | fe2323df999 | 动态码请忽略 
    退款金额 | amount | 是 | int | 12 | 单位（分） 
    接入商原订单号 | oriOrderNo |　 否　  |  String | 201804211320| 
    接入商退款订单号 | orderNo |　 否　  |  String | 201804211320| 接入商不传，默认平台退单号 
    接入商退款唯一订单号 | refundNo |　 是　  |  String | 1712010027-P03936456117297483776| 接入商唯一，退单号 
    明付商户原订单号 | TradeNo | 是 | String | P982127777712 | oriOrderNo ,oriTradeNo 二选一，oriTradeNo
    支付类型 |　payType | 否 |  String  | WX_NATIVE | WX_NATIVE(微信）,ALI_NATIVE（支付宝） 
    退货商品详情 | goodsDetail | 否  |  String |  |  [{"goodsId":"id0","goodsName":"iname","price":3,"quantity":2},{"goodsId":"id1","goodsName":"iname","price":3,"quantity":2}] 
    退单时间 | beginTime | 是 | String  | 20180504123546 | yyyyMMddHHmmss 
     IP地址 | ip | 是|  String | 127.0.0.1 | 
     备注 | comment |　否 |  String |  |  
     签名 |　sign | 是 |   String | | 非空参数按ASCII 排序，末尾增加明付平台分配的KEY  
       
  - 返回结果 
  
       字段名  | 变量名 | 必填 | 类型 | 示例值 |　描述 
      --------| ----- | -----|------| ----- | ---
      原单支付金额  |oriPayMoney | 否 |　int | 1000 |  单位（分），只有查询退款时，该值才有意义
      退款时，原单剩余可退金额 | oriSurplusMoney | 否 | int  | 1000 |  单位（分），只有查询退款时，该值才有意义
      退款时 ，原单接入商订单号 | oriOrderNo | 否 |　String | 201804211320 | 
      退款时，原单明付平台流水号 | oriTradeSerial | 否 | String | P901283232343434 | 
      退款时，原单三方平台流水号 | oriThirdTradeNo | 否 | String | 40092182888812121 |
      总支付金额 | needPayMoney |　 是　  |  int | 1000 | 终端发起的待付金额  needPayMoney=payMoney+thirdDiscountMoney+discountMoney+merchantDiscountMoney
      用户支付金额 | payMoney |　 是　  |  int | 1000 | 用户支付金额 
      第三方优惠金额 | thirdDiscountMoney |　 否　  |  int | 1000 | 第三方优惠金额 
      明付平台优惠金额 | discountMoney |　 否  |  int | 1000 | 明付平台优惠金额  
      接入商优惠金额 | merchantDiscountMoney |　 否　  |  int | 1000 | 暂不支持
      交易时间 | tradeTime |　 是　  |  String | 20180504123546 | yyyyMMddHHmmss   
      接入商订单号 | orderNo | 否 |　String | 201804211320 | 
      明付平台流水号 | tradeSerial | 否 | String | P901283232343434 | 
      三方平台流水号 | thirdTradeNo | 否 | String | 40092182888812121 | 
     
     
    
    


  
 
